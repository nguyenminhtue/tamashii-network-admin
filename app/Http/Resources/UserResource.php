<?php

namespace App\Http\Resources;

use App\Admin;
use App\CustomToken;
use App\Friend;
use App\PropPublicUser;
use App\UserToken;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $data = [];
        $accessToken = $_GET['access_token'];

        $isUserToken = UserToken::select(['accessible', 'user_token'])
            ->where('accessible', 1)
            ->where('user_token', $accessToken)
            ->first();
        $isCustomToken = CustomToken::select(['token'])
            ->where('token', $accessToken)
            ->first();
        $isAdminToken = Admin::where([
            ['token', $accessToken],
        ])->first();

        if ($isUserToken || $isCustomToken || $isAdminToken) {
            if ($isUserToken) {
                $data['name'] = $this->name;
                $data['avatar'] = $this->avatar;
                if ($this->publicProp->phone === 1) {
                    $data['phone'] = $this->phone;
                }
                if ($this->publicProp->gender === 1) {
                    $data['gender'] = $this->gender;
                }
                if ($this->publicProp->dob === 1) {
                    $data['dob'] = $this->dob;
                }
                if ($this->publicProp->email === 1) {
                    $data['email'] = $this->email;
                }
                if ($this->publicProp->description === 1) {
                    $data['description'] = $this->description;
                }

            } else {
                $data['name'] = $this->name;
                $data['avatar'] = $this->avatar;
                $data['phone'] = $this->phone;
                $data['gender'] = $this->gender;
                $data['dob'] = $this->dob;
                $data['email'] = $this->email;
                $data['description'] = $this->description;
            }
        }
        return $data;
    }
}
