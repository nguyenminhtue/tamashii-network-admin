<?php

namespace App\Http\Controllers;

use App\CustomToken;

use App\UserToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class TokenAdminController extends Controller
{
    private $userToken;
    private $cusToken;

    public function __construct(UserToken $userToken, CustomToken $cusToken)
    {
        $this->userToken = $userToken;
        $this->cusToken = $cusToken;
    }

    public function index()
    {

        $userToken = $this->userToken->paginate(paginate);
        $cusToken = $this->cusToken->paginate(paginate);

        if ($cusToken->currentPage() > 1) {
            Session::flash('tabCusToken', 'active');
        }
        return view('admin.token.index', ['userToken' => $userToken, 'cusToken' => $cusToken]);
    }

    public function updateToken(Request $request)
    {
        $data = CustomToken::find($request->id);
        $data->expired_at = $request->expired_at;
        $data->save();
        return redirect()->route('admin.token.index')->with('messageUpdateTime', 'Update time success!');
    }

    public function statusUser(Request $request, $id)
    {
        $user = UserToken::find($id);
        $user->accessible = $request->accessible;
        $user->save();
        return redirect()->route('admin.token.index')->with('messageSession', 'Update success!');
    }

    public function addToken()
    {
        $dt = Carbon::now('Asia/Ho_Chi_Minh');
        $dt->addMinute(10);

        $table_status = CustomToken::all();
        if (!$table_status->isEmpty()) {
            $next_id = $table_status[0]->Auto_increment;
        } else {
            $next_id = 1;
        }

        $newToken = new CustomToken();
        $newToken->token = Hash::make($next_id);
        $newToken->expired_at = $dt;
        $newToken->save();
        return redirect()->route('admin.token.index')->with('message', 'Create token success!');
    }
}
