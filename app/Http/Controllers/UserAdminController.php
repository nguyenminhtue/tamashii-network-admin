<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;

class UserAdminController extends Controller

{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {

        $data = $this->user->paginate(paginate);
        return view('admin.user.index', compact('data'));
    }

    public function detail($id)
    {
        $user = User::query()->findOrFail($id);
        $propPublic = $user->propPublic;
        $token = $user->userToken->user_token;
        return view('admin.user.detail', compact('user', 'propPublic', 'token'));

    }
}
