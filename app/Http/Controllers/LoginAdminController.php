<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginAdminController extends Controller
{
    public function login()
    {
        return view('admin.login');
    }

    public function checkLogin(LoginRequest $request)
    {
        $email = Admin::where('email', $request->email)->first();
        $password = Admin::where('password', $request->password)->first();
        $check = $request->only('email', 'password');

        if (Auth::guard("admins")->attempt($check)) {
            return redirect()->route('admin.home');
        } else {
            if (!$password) {
                if ($email) {
                    return redirect()->route('admin.login')
                        ->withErrors(['message' => 'Password is incorrect!']);
                } else {
                    return redirect()->route('admin.login')
                        ->withErrors(['message' => 'The account is not registered!']);
                }
            }
        }

    }

    public function logout()
    {
        Auth::guard('admins')->logout();
        return redirect()->route('admin.login');
    }
}
