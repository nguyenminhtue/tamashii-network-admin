<?php

namespace App\Http\Controllers\api;

use App\Admin;
use App\CustomToken;
use App\Friend;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckApiRequest;
use App\User;
use App\UserToken;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->profile_id && !$request->access_token) {
            return response()->json([
                'message' => 'The profile_id and access_token are required.',
            ], 404);
        }
        if (!$request->profile_id) {
            return response()->json([
                'message' => 'The profile_id is required.',
            ], 404);
        }
        if (!$request->access_token) {
            return response()->json([
                'message' => 'The access_token is required.',
            ], 404);
        }

        $profileId = $_GET['profile_id'];
        $accessToken = $_GET['access_token'];
        $user = User::find($profileId);


        $isAdminToken = Admin::where([
            ['token', $accessToken],
        ])->first();

        $isUserToken = UserToken::select(['accessible', 'user_token'])
            ->where('accessible', 1)
            ->where('user_token', $accessToken)
            ->first();

        $isCustomToken = CustomToken::select(['token'])
            ->where('token', $accessToken)
            ->first();

        if ($isCustomToken) {
            $dateNow = Carbon::now();
            $expriedToken = CustomToken::where('token', $accessToken)->first();
            $dateExpired = $expriedToken->expired_at;
            $checkExpried = $dateNow->lte($dateExpired);
        }

        if ($isUserToken) {
            $getFriendId = UserToken::where('user_token', $accessToken)->first();
            $getFriendId = $getFriendId->user_id;

            $checkFriend = Friend::where([
                ['friend_id', '=', $getFriendId],
                ['user_id', '=', $profileId],
                ['accepted_request_friend', '=', 1],
                ['accepted_api', '=', 1],
            ])->first();
        }

        if (($user && $isUserToken && $checkFriend) || ($user && $isCustomToken && ($checkExpried == true)) || $user && $isAdminToken) {
            $datas = new UserResource($user);
            return response()->json([
                'message' => 'Success!',
                'data' => $datas,
            ], 200);
        } elseif ($user) {
            if (!$isAdminToken && !$isCustomToken && !$isUserToken) {
                return response()->json([
                    'message' => "Token don't exist or blocked!",
                ], 404);
            } elseif ($isUserToken && !$checkFriend) {
                return response()->json([
                    'message' => "Not friend or don't accept api!",
                ], 404);
            } elseif ($isCustomToken && $checkExpried == false) {
                return response()->json([
                    'message' => "Token expired!",
                ], 404);

            }
        } elseif (!$user && ($isAdminToken || $isCustomToken || $isUserToken)) {
            return response()->json([
                'message' => "Id don't exist!",
            ], 404);
        } else {
            return response()->json([
                'message' => "Id and token don't exist!",
            ], 404);
        }
    }
}
