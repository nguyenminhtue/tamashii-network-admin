<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomToken extends Model
{
    protected $table = 'custom_tokens';
    protected $fillable = ['id','token','expried_at'];
}
