<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = 'user_tokens';
    protected $fillable = ['id','user_token','accessible'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}
