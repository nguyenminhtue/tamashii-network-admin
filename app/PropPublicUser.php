<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropPublicUser extends Model
{
    protected $table = 'prop_public_user';
}