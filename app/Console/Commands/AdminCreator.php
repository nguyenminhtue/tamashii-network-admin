<?php

namespace App\Console\Commands;

use App\Admin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminCreator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $email = 'admin@gmail.com';
    protected $password = '123456';
    protected $signature = 'admin:create';
    

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create admin account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $admin = Admin::where('email', $this->email)->first();
        if ($admin) {
            $this->error('User already existed!');
        } else {
            $table_status = DB::select("SHOW TABLE STATUS LIKE 'admins'");
            $next_id = $table_status[0]->Auto_increment;
            Admin::insert([
                'email' => $this->email,
                'password' => Hash::make($this->password),
                'token' => Hash::make($next_id)
            ]);
            $this->info('User successfully created!');
        }
        
    }
}
