<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect()->route('admin.home');
});

Route::get('/login',['as' => 'admin.login', 'uses' => 'LoginAdminController@login']);
Route::get('/logout',['as' => 'admin.logout', 'uses' => 'LoginAdminController@logout']);

Route::post('/check-login',['as' => 'admin.check_login', 'uses' => 'LoginAdminController@checkLogin']);


Route::group(['prefix' => '/admin', 'middleware'=>'auth:admins'],function (){
    Route:: get('',['uses' => 'HomeAdminController@index']);
    Route:: get('/home',['as'=>'admin.home','uses' => 'HomeAdminController@index']);
    Route::group(['prefix' => '/user'],function (){
            Route::get('/list',['as' => 'admin.user.index','uses' => 'UserAdminController@index']);

            Route::get('/detail/{id}',['as' => 'admin.user.detail','uses' => 'UserAdminController@detail']);
    });
    Route::group(['prefix' => '/token'],function (){
            Route::get('/list',['as' => 'admin.token.index','uses' => 'TokenAdminController@index']);
            Route::post('/statusUser/{id}',['as' => 'admin.token.statusUser','uses' => 'TokenAdminController@statusUser']);
            Route::get('/addToken',['as'=>'admin.addtoken','uses'=>'TokenAdminController@addToken']);
            Route::post('/updateToken',['as'=>'admin.updateToken','uses'=>'TokenAdminController@updateToken']);

    });
});


