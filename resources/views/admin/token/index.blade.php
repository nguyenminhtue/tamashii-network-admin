@extends('layouts.admin')

@section('title')
    <title xmlns="">User</title>
@endsection

@section('content')

    <div class="content-wrapper">

        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <div class="card card-primary card-tabs">
                                    <div class="card-header p-0 pt-1">
                                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link  active " id="custom-tabs-one-home-tab"
                                                   data-toggle="pill" href="#custom-tabs-one-home" role="tab"
                                                   aria-controls="custom-tabs-one-home" aria-selected="true">User
                                                    token</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link " id="custom-tabs-one-profile-tab"
                                                   data-toggle="pill" href="#custom-tabs-one-profile" role="tab"
                                                   aria-controls="custom-tabs-one-profile" aria-selected="false">Custom
                                                    token</a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-one-tabContent">
                                            <div class="tab-pane fade show  active " id="custom-tabs-one-home"
                                                 role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                                <table class="table table-hover table-bordered" id="table_id">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col" style="width: 6%">Id</th>
                                                        <th scope="col">Username</th>
                                                        <th>Status</th>
                                                        <th style="width: 10%">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @forelse($userToken as $data)
                                                        <tr>
                                                            <th scope="row" style="text-align: center">{{$data->id}}</th>
                                                            <td>{{$data->user->name}}</td>
                                                            <td>
                                                                {{$data->accessible == 0 ? 'Lock' : 'UnLock'}}
                                                            </td>
                                                            <td>
                                                                <form action="{{URL::to('admin/token/statusUser/'. $data->id)}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <select hidden class="text align-content-center "
                                                                            name="accessible" id="" >
                                                                        <option value="0" @if($data->accessible == 1) selected @endif>Lock</option>
                                                                        <option value="1"@if($data->accessible !== 1) selected @endif>Unlock</option>
                                                                    </select>
                                                                    <button class="btn btn-success float-right"
                                                                            type="submit">Change
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @empty
                                                        <td>No data</td>
                                                        <td>No data</td>
                                                        <td>No data</td>
                                                        <td>No action</td>
                                                    @endforelse
                                                    </tbody>

                                                </table>
                                                <div class="pagination mt-3 float-right">
                                                    {{ $userToken->links() }}
                                                </div>
                                            </div>

                                            <div class="tab-pane fade " id="custom-tabs-one-profile" role="tabpanel"
                                                 aria-labelledby="custom-tabs-one-profile-tab">
                                                <table class="table ">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Id</th>
                                                        <th scope="col">Token</th>
                                                        <th scope="col">Expired_at</th>
                                                        <th scope="col">Action</th>
                                                        <a href="{{route('admin.addtoken')}}" class="btn btn-success mb-2"><i class="fa fa-plus-circle"></i> Add token</a>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($cusToken as $data)
                                                        <form action="{{route('admin.updateToken')}}"  method = "post">
                                                            @csrf
                                                            <tr>
                                                                <th scope="row">{{$data->id}}</th>
                                                                <td>{{$data->token}}</td>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <input type="text" name="id" value="{{$data->id}}" hidden>
                                                                        <input class="textbox-n" type="datetime-local" value="{{date('Y-m-d\TH:i', strtotime($data->expired_at))}}" name="expired_at">
                                                                    </div>
                                                                </td>
                                                                <td><button class="btn btn-warning"><i class="fa fa-edit"></i> Update</button></td>
                                                            </tr>
                                                        </form>
                                                    @empty
                                                        <td>No data</td>
                                                        <td>No data</td>
                                                        <td>No data</td>
                                                    @endforelse
                                                    </tbody>
                                                </table>
                                                <div class="pagination mt-3 float-right">
                                                {{ $cusToken->links() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>


@endsection
@section('js')
    <script type="text/javascript">
                @if(Session::has('message'))
        const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
        Toast.fire({
            icon: 'success',
            title: 'Create token success!'
        })
        @endif
    </script>


    <script type="text/javascript">
                @if(Session::has('messageSession'))
        const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
        Toast.fire({
            icon: 'success',
            title: 'Update success!'
        })
        @endif
    </script>

    <script type="text/javascript">
                @if(Session::has('messageUpdateTime'))
        const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
        Toast.fire({
            icon: 'success',
            title: 'Update time success!'
        })
        @endif
    </script>

    <script>
        $(document).ready(function () {
            $('a[data-toggle="pill"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#custom-tabs-one-tab a[href="' + activeTab + '"]').tab('show');
            }
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#table_id').DataTable({
                info: false,
                paging: false
            });

        });
    </script>
@endsection

