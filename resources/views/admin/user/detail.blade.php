@extends('layouts.admin')

@section('title')
    <title>User</title>
@endsection

@section('content')

    <div class="content-wrapper">

        <div class="content">

            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card mt-3">
                            <div class="card-header bg-dark text-white">
                                <h3>User name: {{ $user->name }}</h3>
                                <h3>User token: {{ $token }}</h3>
                            </div>
                            <div class="row">
                                <div class="card-body">
                                    <h4>Public Information</h4>
                                    <hr>
                                    <br> 

                                    @if ($propPublic->phone === 1)
                                        <h5>Phone</h5>
                                        <p class="card-text">
                                            {{ $user->phone }} 
                                        </p>
                                    @endif

                                    @if ($propPublic->gender === 1)
                                        <h5>Gender</h5>
                                        <p class="card-text">
                                            @if ($user->gender == FEMALE)
                                                Female
                                            @elseif($user->gender == MALE)
                                                Male
                                            @else
                                                Unisex
                                            @endif
                                        </p>
                                    @endif

                                    @if ($propPublic->dob === 1)
                                        <h5>Dob</h5>
                                        <p class="card-text">
                                                {{ $user->dob }}
                                        </p>
                                    @endif

                                    @if ($propPublic->email === 1)
                                        <h5>Email</h5>
                                        <p class="card-text">
                                            {{ $user->email }}
                                        </p>
                                    @endif

                                    @if ($propPublic->description === 1)
                                        <h5>Description</h5>
                                        <p class="card-text">
                                                {{ $user->description}}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>


@endsection

