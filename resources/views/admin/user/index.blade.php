@extends('layouts.admin')

@section('title')
    <title>User</title>
@endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>User List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                            <li class="breadcrumb-item active">User List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered" id="table_id">
                            <thead>
                            <tr>
                                <th scope="col" style="width: 6%">Id</th>
                                <th scope="col">Username</th>
                                <th scope="col">Email</th>
                                <th style="text-align: center; width: 10%">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td scope="row" style="text-align: center">{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td style="text-align: center">
                                        <a href="{{ URL::to("admin/user/detail/".$item->id) }}" class="btn btn-success" title="Show info"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination float-right mt-3">
                            {{ $data->links() }}
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#table_id').DataTable({
                info: false,
                paging: false
            });
        });
    </script>
@endsection